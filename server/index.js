const express = require("express");
const todoRouter = require("./routers/todo");
const cors = require("cors");
const { json } = require("body-parser");

const PORT = process.env.PORT || 3001;

const app = express();

app.use(cors());
app.use(json());

app.use("/api/todos", todoRouter);

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
