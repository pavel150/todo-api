const client = require("../db/db");
const { Router } = require("express");

const router = Router();

const TODOS_KEY = "TODOS";

router.get("", async (req, res) => {
  try {
    console.log("get all");
    const all = await client.get(TODOS_KEY);
    console.log(all);
    if (all) {
      res.json({ todos: all });
    } else {
      res.status(404).send();
    }
  } catch (error) {
    res.status(500).send();
  }
});

router.put("", async (req, res) => {
  try {
    const todos = req.body;
    console.log("porst", todos);
    if (todos) {
      client.set(TODOS_KEY, JSON.stringify(todos));
      res.status(201).send();
    }
  } catch (error) {
    res.status(500).send();
  }
});

router.delete("", async (req, res) => {
  try {
    const result = await client.del(TODOS_KEY);
    console.log("delete", result);
    res.status(200).send();
  } catch (error) {
    res.status(500).send();
  }
});

module.exports = router;
