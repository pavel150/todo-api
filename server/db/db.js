const client = require("redis").createClient(
  process.env.DB_URL
    ? {
        url: process.env.DB_URL,
      }
    : undefined
);

client.connect();

client.on("error", function (err) {
  console.log("Error " + err);
});

module.exports = client;
